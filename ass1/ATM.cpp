/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#include "ATM.h"
#include <iostream>
#include <ctime>
#include <iomanip>
using namespace std;

ATM::ATM()
{
    Aid= "\0";
    fifty=0;
    twenty=0;
}
void ATM::setAid(string atm)
{
    Aid=atm;
}
void ATM::setfifty(int f)
{
    fifty=f;
}
void ATM::settwenty(int t)
{
    twenty=t;
}
string ATM::getAid()
{
    return Aid;
}
int ATM::getfifity()
{
    return fifty;
}
int ATM::gettwenty()
{
    return twenty;
}
void ATM::load(ifstream &file, ATM *record, int num)
{
    string atm;
    int f;
    int t;
    char comma;
    for(int i=0;i<num;i++)
    {
        file.ignore();
        getline(file,atm,',');
        record[i].setAid(atm);
        file>>f;
        record[i].setfifty(f);
        file>>comma;
        file>>t;
        record[i].settwenty(t);
    }
//    for(int i=0;i<num;i++)
//    {
//        cout<<record[i].getAid()<<"\t"<<record[i].getfifity()<<endl;
//    }
//    cout<<num<<endl;
    cout<<"ATMs have been loaded"<<endl;
}

void ATM::addNote(ATM *data, int num)
{
    string Aid;
    int type;
    int amount;
    bool find=false;
    cout<<"ATM ID: ";
    cin>>Aid;
    cout<<"Note type(1-$50, 2-$20): ";
    cin>>type;
    cout<<"Amount: ";
    cin>>amount;
    ofstream fout;
    fout.open("/Users/jiangxinwei/Desktop/ATM.log",ios::out|ios::app);
    for(int i=0;i<num;i++)
    {
        if(data[i].getAid()==Aid)
        {
            find=true;
            int balance=amount+data[i].getfifity()+data[i].gettwenty();
            time_t timer=time(NULL);
            tm t=*localtime(&timer);
            int day=t.tm_mday;
            int month=t.tm_mon+1;
            int year=t.tm_year+1900;
            int hour=t.tm_hour;
            int min=t.tm_min;
            int second=t.tm_sec;
            if(type==1)
            {
                data[i].setfifty(data[i].getfifity()+amount);
                fout<<Aid<<", "<<setw(2)<<setfill('0')<<day<<"/"<<setw(2)<<setfill('0')<<month<<"/"<<year<<" "<<hour<<":"<<min<<":"<<second<<", "<<"Add $50 notes, "<<amount<<", "<<balance<<endl;
            }
            else if(type==2)
            {
                data[i].settwenty(data[i].gettwenty()+amount);
                fout<<Aid<<", "<<setw(2)<<setfill('0')<<day<<"/"<<setw(2)<<setfill('0')<<month<<"/"<<year<<" "<<hour<<":"<<min<<":"<<second<<", "<<"Add $20 notes, "<<amount<<", "<<balance<<endl;
            }
            else
            {
                cout<<"Please choose the type."<<endl;
            }
        }
    }
    if(find==false)
    {
        cout<<"Sorry, we cannot find this ATM ID."<<endl;
    }
    fout.close();
}
void ATM::saveATM(ATM *data, int count, ofstream &fwrite)
{
    fwrite<<count<<endl;
    for(int i=0;i<count;i++)
    {
        fwrite<<data[i].getAid()<<","<<data[i].getfifity()<<","<<data[i].gettwenty()<<endl;
    }
}