/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#ifndef ATM_h
#define ATM_h

#include <stdio.h>
#include <fstream>
#include <string>

class ATM
{
    private:
        std::string Aid;
        int fifty;
        int twenty;
    public:
        ATM();
        void setAid(std::string);
        void setfifty(int );
        void settwenty(int );
        std::string getAid();
        int getfifity();
        int gettwenty();
        void load(std::ifstream &, ATM *, int);
        void addNote(ATM *, int);
        void saveATM(ATM *, int, std::ofstream &);
};

#endif /* ATM_h */
