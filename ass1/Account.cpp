/*
 written by: XINWEI JIANG 
 student number: 4951207
 */

#include "Account.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;

Account::Account()
{
    cid= "\0";
    BSB= "\0";
    account= "\0";
    strcpy(fname, "\0");
    strcpy(lname, "\0");
    balance=0;
    withdraw=0;
}
void Account::setcid(string ID)
{
    cid = ID;
}
void Account::setBSB(string Bnum)
{
    BSB = Bnum;
}
void Account::setaccount(string acc)
{
    account=acc;
}
void Account::setfname(char first[])
{
    strcpy(fname,first);
}
void Account::setlname(char last[])
{
    strcpy(lname,last);
}
void Account::setbalance(float b)
{
    balance=b;
}
void Account::setwithdraw(int w)
{
    withdraw=w;
}
string Account::getcid()
{
    return cid;
}
string Account::getBSB()
{
    return BSB;
}
string Account::getaccount()
{
    return account;
}
char *Account::getfname()
{
    return fname;
}
char *Account::getlname()
{
    return lname;
}
float Account::getbalance()
{
    return balance;
}
int Account::getwithdraw()
{
    return withdraw;
}
void Account::loadRecord(ifstream &fin, Account *record, int num)
{
    //Account *record;
    //record = new Account[10];
    string ID;
    string Bnum;
    char acc[10];
    char first[20];
    char last[20];
    char ba[10];
    float b;
    fin.ignore();
    for(int i=0;i<num;i++)
    {
        getline(fin,ID,',');
        record[i].setcid(ID);
        //cout<<"~~~"<<this->getcid()<<endl;
        getline(fin,Bnum,',');
        record[i].setBSB(Bnum);
        fin.getline(acc,10,',');
        record[i].setaccount(acc);
        fin.getline(first,20,',');
        record[i].setfname(first);
        fin.getline(last,20,',');
        record[i].setlname(last);
        fin.getline(ba,10,'\n');
        b=atof(ba);
        record[i].setbalance(b);
    }
    cout<<"Accounts have been loaded"<<endl;
}

void Account::withdrawMoney(Account *record, int num, ATM *data, int count)
{  //num:the number of Accounts, count: the number of ATMs
    string Bnum;
    string acc;
    int amount;
    cout<<"BSB: ";
    cin>>Bnum;
    cout<<"Account: ";
    cin>>acc;
    cout<<"Amount: ";
    cin>>amount;
    bool find = false;
    int fnum=0;      //the amount of $50
    int tnum=0;      //the amount of $20
    float b;
    for(int i=0;i<num;i++)
    {
        if(Bnum==record[i].getBSB() && acc==record[i].getaccount())
        {
            if(record[i].getbalance()>=amount)
            {
                fnum=amount/50;
                int left=amount-fnum*50;   //the cash after the required money minus fnum*50;
                while(left%20!=0&&fnum>=0)
                {
                    fnum=fnum-1;
                    left=amount-fnum*50;
                }
                if(fnum>=0)
                {
                    record[i].setwithdraw(amount);
                    tnum=left/20;
                    b=record[i].getbalance()-amount;
                    record[i].setbalance(b);
                    int index=rand()%count;
                    data[index].setfifty(data[index].getfifity()-fnum*50);
                    data[index].settwenty(data[index].gettwenty()-tnum*20);
                    if(data[index].getfifity()<2000)
                    {
                        cout<<"Please add more notes for ATM $50 note box"<<endl;
                    }
                    if(data[index].gettwenty()<2000)
                    {
                        cout<<"Please add more notes for ATM $20 note box"<<endl;
                    }
                    time_t timer=time(NULL);
                    tm t=*localtime(&timer);
                    int day=t.tm_mday;
                    int month=t.tm_mon+1;
                    int year=t.tm_year+1900;
                    int hour=t.tm_hour;
                    int min=t.tm_min;
                    ofstream fout;
                    fout.open("/Users/jiangxinwei/Desktop/ATM.log",ios::out|ios::app);
                    cout<<"You have "<<fnum<<"*$50 notes, "<<tnum<<"*$20 notes. Your balance is $"<<record[i].getbalance()<<endl;
                    fout<<data[index].getAid()<<", "<<setw(2)<<setfill('0')<<day<<"/"<<setw(2)<<setfill('0')<<month<<"/"<<year<<" "<<hour<<":"<<min<<", withdraw, "<<record[i].getBSB()<<", "<<record[i].getaccount()<<", "<<record[i].getwithdraw()<<"."<<endl;
                    fout.close();
                }
                else
                {
                    cout<<"The amount of cash cannot be withdrawn."<<endl;
                }
            }
            else
            {
                cout<<"Sorry, the required cash is bigger than the balance."<<endl;
            }
            find=true;
        }
    }
    if(find==false)
    {
        cout<<"Sorry, we cannot find your account."<<endl;
    }
}

void Account::find(Account *record, int num)
{
    string Bnum;
    string acc;
    cout<<"BSB: ";
    cin>>Bnum;
    cout<<"Account: ";
    cin>>acc;
    bool find=false;
    for(int i=0;i<num;i++)
    {
        if(Bnum==record[i].getBSB() && acc==record[i].getaccount())
        {
            cout<<"Your balance is $"<<record[i].getbalance()<<endl;
            find=true;
        }
    }
    if(find==false)
    {
        cout<<"Sorry, we cannot find your account."<<endl;
    }
}

void Account::save(Account *record, int num, ofstream &fout)
{
    fout<<num<<endl;
    for(int i=0;i<num;i++)
    {
        fout<<record[i].getcid()<<","<<record[i].getBSB()<<","<<record[i].getaccount()<<","<<record[i].getfname()<<","<<record[i].getlname()<<","<<record[i].getbalance()<<endl;
    }
}





