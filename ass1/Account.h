/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#ifndef Account_h
#define Account_h

#include <iostream>
#include <fstream>
#include <string>
#include "ATM.h"

class Account
{
    private:
        std::string cid;
        std::string BSB;
        std::string account;
        char fname[20];
        char lname[20];
        float balance;
        int withdraw;
    public:
        Account();
        void setcid(std::string);
        void setBSB(std::string);
        void setaccount(std::string);
        void setfname(char []);
        void setlname(char []);
        void setbalance(float );
        void setwithdraw(int );
        std::string getcid();
        std::string getBSB();
        std::string getaccount();
        char *getfname();
        char *getlname();
        float getbalance();
        int getwithdraw();
        void loadRecord(std::ifstream &, Account *, int );
        void withdrawMoney(Account *,int ,ATM *,int );
        void find(Account *, int);
        void save(Account *,int ,std::ofstream &);
};

#endif /* Account_h */
