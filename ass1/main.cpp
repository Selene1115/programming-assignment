/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#include <iostream>
#include <fstream>
#include <string>
#include "Account.h"
#include "ATM.h"
using namespace std;

//void withdraw(Account *, int);
//void find(Account *, int);

int main(/*int argc, char *argv[]*/)
{
//    if(argc!=3)
//    {
//        cerr<<"Something wrong"<<endl;
//        return 1;
//    }
    
    ifstream fin;
    ifstream file;
    fin.open(/*argv[1]*/"/Users/jiangxinwei/Desktop/accounts.txt");
    file.open(/*argv[2]*/"/Users/jiangxinwei/Desktop/ATM.txt");
    int num;                    //the number of Accounts
    fin>>num;
    Account *record;
    record=new Account[num];
    record->loadRecord(fin,record,num);
//    for(int i=0;i<num;i++)
//    {
//        cout<<record[i].getcid()<<"\t"<<record[i].getBSB()<<endl;
//    }
//    cout<<num<<endl;
    int count=0;         //the number of ATMs
    file>>count;
    ATM *data;
    data = new ATM[count];
    data->load(file, data,count);
//    for(int i=0;i<count;i++)
//    {
//        cout<<data[i].getAid()<<"\t"<<data[i].getfifity()<<endl;
//    }
//    cout<<count<<endl;
    int choice=0;
    while(choice!=4)
    {
        cout<<"1.Withdraw"<<endl;
        cout<<"2.Find balance"<<endl;
        cout<<"3.Add note"<<endl;
        cout<<"4.Quit"<<endl;
        cout<<"Please choose: ";
        cin>>choice;
        cin.ignore();
        switch(choice)
        {
            case 1: record->withdrawMoney(record, num, data, count);
                break;
            case 2: record->find(record, num);
                break;
            case 3: data->addNote(data, count);
            case 4: ofstream fout;
                ofstream fwrite;
                fout.open("/Users/jiangxinwei/Desktop/accounts1.txt"/*argv[1]*/);
                fwrite.open(/*argv[2]*/"/Users/jiangxinwei/Desktop/ATM1.txt");
                record->save(record,num,fout);
                data->saveATM(data, count, fwrite);
                fout.close();
                fwrite.close();
                cout<<"Thank you."<<endl;
                break;
        }
    }
    delete [] record;
    delete [] data;
}
