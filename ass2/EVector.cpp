/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#include "EVector.h"
#include<iostream>
using namespace std;

istream& operator>>(istream &in, EVector &vector)
{
    in>>vector.dimension;
    vector.tuples=new double [vector.dimension];
    double tmpin[vector.dimension];
    for(int i=0;i<vector.dimension;i++)
    {
        in>>tmpin[i];
        vector.tuples[i]=tmpin[i];
    }
    return in;
}
ostream& operator<<(ostream& out, const EVector &vector)
{
    for(int i=0;i<vector.dimension-1;i++)
    {
        out<<vector.tuples[i]<<",";
    }
    out<<vector.tuples[vector.dimension-1];
    return out;
}
EVector::EVector()
{
    tuples=NULL;
    dimension=0;
}
EVector::~EVector()
{
    if(tuples!=NULL)
    {
        delete [] tuples;
    }
    tuples=NULL;
}
EVector::EVector(const EVector &tup)
{
    dimension=tup.dimension;
    tuples=new double [dimension];
    for(int i=0;i<dimension;i++)
    {
        tuples[i]=tup.tuples[i];
    }
}
EVector EVector::operator=(const EVector &vector)
{
    if(this==&vector)
    {
        return *this;
    }
    if(tuples!=NULL)
    {
        delete [] tuples;
    }
    dimension=vector.dimension;
    tuples=new double[dimension];
    for(int i=0;i<dimension;i++)
    {
        tuples[i]=vector.tuples[i];
    }
    return *this;            
}
EVector EVector::operator+(const EVector&vector)
{
    EVector sum;
    if(dimension!=vector.dimension)
    {
        cout<<"Cannot add these two vectors!"<<endl;
    }
    else
    {
//        EVector sum;
        sum.dimension=dimension;
        sum.tuples=new double[sum.dimension];
        for(int i=0;i<dimension;i++)
        {
            sum.tuples[i]=tuples[i]+vector.tuples[i];
        }
        //return sum;
    }
    return sum;
}
EVector EVector::operator-(const EVector&vector)
{
    EVector result;
    if(dimension!=vector.dimension)
    {
        cout<<"Cannot minus these two vectors!"<<endl;
        //return -1;
    }
    else
    {
        //EVector result;
        result.dimension=dimension;
        result.tuples=new double[result.dimension];
        for(int i=0;i<result.dimension;i++)
        {
            result.tuples[i]=tuples[i]-vector.tuples[i];
        }
    }
    return result;
}
EVector EVector::operator*(const EVector&vector)
{
    EVector result;
    if(dimension!=vector.dimension)
    {
        cout<<"Cannot multiple these two vectors!"<<endl;
    }
    else
    {

        result.dimension=1;
        result.tuples=new double[result.dimension];
        double vec[dimension];
        for(int i=0;i<dimension;i++)
        {
            vec[i]=tuples[i]*vector.tuples[i];
            result.tuples[0]=result.tuples[0]+vec[i];
        }
    }
    return result;
}
EVector operator*(double d, const EVector&vector)
{
    EVector tmp;
    tmp.dimension=vector.dimension;
    tmp.tuples=new double[tmp.dimension];
    for(int i=0;i<tmp.dimension;i++)
    {
        tmp.tuples[i]=d*vector.tuples[i];
    }
    return tmp;
}
EVector EVector::operator*(double d)
{
    EVector result;
    result.dimension=dimension;
    result.tuples=new double[result.dimension];
    for(int i=0;i<dimension;i++)
    {
        result.tuples[i]=d*tuples[i];
    }
    return result;
}
EVector EVector::operator/(double d)
{
    EVector result;
    result.dimension=dimension;
    result.tuples=new double[result.dimension];
    for(int i=0;i<dimension;i++)
    {
        result.tuples[i]=tuples[i]/d;
    }
    return result;
}















