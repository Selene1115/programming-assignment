/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#ifndef EVector_h
#define EVector_h

#include <iostream>
#include <fstream>

class EVector
{
    friend std::istream& operator>>(std::istream&, EVector &);
    friend std::ostream& operator<<(std::ostream&, const EVector &);
    private:
        double *tuples;
        int dimension;
    public:
        EVector();
        ~EVector();
        EVector(const EVector &);
        //EVector(int);
        EVector operator=(const EVector&);
        EVector operator+(const EVector&);
        EVector operator-(const EVector&);
        EVector operator*(const EVector&);
        friend EVector operator*(double, const EVector&);
        //EVector operator*(double, const EVector&);
        EVector operator*(double);
        EVector operator/(double);
};

#endif /* EVector_h */
