/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#include <iostream>
#include "EVector.h"
using namespace std;

int main()
{
    //int dimen1=0;
    cout<<"Input dimension and tuples for a Euclidean vector v1: ";
//    cin>>dimen1;
    EVector vector1;
    cin>>vector1;
    cout<<"Euclidean vector v1 = (";
    cout<<vector1;
    cout<<")"<<endl;
    //int dimen2=0;
    cout<<"Input dimension and tuples for a Euclidean vector v2: ";
//    cin>>dimen2;
    EVector vector2;
    cin>>vector2;
    cout<<"Euclidean vector v2 = (";
    cout<<vector2;
    cout<<")"<<endl;
    EVector vector3;
    vector3=vector1+vector2;
    cout<<"v3 = v1 + v2 = (";
    cout<<vector3;
    cout<<")"<<endl;
    vector3=vector1-vector2;
    cout<<"v3 = v1 - v2 = (";
    cout<<vector3;
    cout<<")"<<endl;
    vector3=vector1*vector2;
    cout<<"v3 = v1 * v2 = ";
    cout<<vector3<<endl;
    cout<<"Input a double value for d: ";
    double d=0;
    cin>>d;
    vector3=d*vector1;
    cout<<"d * v1 = "<<d<<" * ("<<vector1<<") = ("<<vector3<<")"<<endl;
    vector3=vector1*d;
    cout<<"v1 * d = "<<"("<<vector1<<")"<<" * "<<d<<" = ("<<vector3<<")"<<endl;
    vector3=vector1/d;
    cout<<"v1 / d = "<<"("<<vector1<<")"<<" / "<<d<<" = ("<<vector3<<")"<<endl;
}







