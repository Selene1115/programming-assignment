/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#ifndef MyList_h
#define MyList_h

#include<iostream>
template <class T>
class Node
{
    private:
        T data;
        Node *next;
    public:
        Node();
        Node(T &);
        void setNext(Node *);
        Node *getNext();
        void setdata(T );
        T getdata();
};
template <class T>
Node<T>::Node()
{
    //data=0;
    next=NULL;
}
template <class T>
Node<T>::Node(T &d)
{
    data=d;
}
template <class T>
void Node<T>::setNext(Node *n)
{
    next=n;
}
template <class T>
Node<T>* Node<T>::getNext()
{
    return next;
}
template <class T>
void Node<T>::setdata(T d)
{
    data=d;
}
template <class T>
T Node<T>::getdata()
{
    return data;
}

template <class T>
class MyList
{
private:
    Node<T> *head;
    Node<T> *tail;
public:
    MyList();
    ~MyList();
    MyList(const MyList &);
    void push_front(const T );
    void push_back(const T );
    bool isEmpty();
    T front();
    T back();
    void pop_front();
    void pop_back();
    
//    class iterator;
//    friend class iterator;
    class iterator
    {
        private:
            Node<T> *current;
            Node<T> *last;
        public:
            iterator();
            T operator*();
            void setcurrent(Node<T> *);
            void setlast(Node<T> *);
            T getcurrent();             //test
            T getlast();                //test
            iterator operator++(int );
            bool operator!=(iterator );
    };
    iterator begin();
    iterator end();
    void print(std::ostream &);
};
template <class T>
MyList<T>::MyList()
{
    head=NULL;
    tail=NULL;
}
template <class T>
MyList<T>::~MyList()
{
    Node<T> *node;
    //node=new Node<T>;
    node=head;
    while(node!=NULL)
    {
        head=node->getNext();
        delete node;
        node=head;
    }
    head=NULL;
    tail=NULL;
}
template <class T>
MyList<T>::MyList(const MyList &list)
{
    if(list.head==NULL)
    {
        head=NULL;
        tail=NULL;
    }
    else
    {
        Node<T> *copy;
        copy=list.head;
        Node<T> *node;
        node=new Node<T>;
        node->setdata(copy->getdata());
        head=node;
        copy=copy->getNext();
        while(copy!=NULL)
        {
            Node<T> *newnode;
            newnode=new Node<T>;
            newnode->setdata(copy->getdata());
            node->setNext(newnode);
            node=node->getNext();
            copy=copy->getNext();
        }
        tail=node;
    }
}
template <class T>
bool MyList<T>::isEmpty()
{
    bool empty=false;
    if(head==NULL)
    {
        empty=true;
    }
    return empty;
}
template <class T>
void MyList<T>::push_front(const T d)
{
    Node<T> *node;
    node=new Node<T>;
    node->setdata(d);
    if(isEmpty())
    {
        head=node;
        tail=node;
    }
    else
    {
        node->setNext(head);
        head=node;
    }
}
template <class T>
void MyList<T>::push_back(const T d)
{
    Node<T> *node;
    node=new Node<T>;
    node->setdata(d);
    if(isEmpty())
    {
        head=node;
        tail=node;
    }
    else
    {
        tail->setNext(node);
        tail=node;
    }
}
template <class T>
T MyList<T>::front()
{
    T temp;
    if(head!=NULL)
    {
        //temp->setdata(*head);
        temp=head->getdata();
    }
    return temp;
}
template <class T>
T MyList<T>::back()
{
    T temp;
    if(tail!=NULL)
    {
        //temp=*tail;
        temp=tail->getdata();
    }
    return temp;
}
template <class T>
void MyList<T>::pop_front()
{
    Node<T> *remove=head;
    if(remove!=NULL)
    {
        //head=remove++;
        head=remove->getNext();
        delete remove;
        if(head==NULL)
        {
            tail=NULL;
        }
    }
}
template <class T>
void MyList<T>::pop_back()       //!!!!!delete????
{
    Node<T> *remove=head;
    if(head!=tail)
    {
        while(remove->getNext()!=tail)
        {
            std::cout<<"!!!!"<<std::endl;
            remove=remove->getNext();
        }
        delete tail;
        //std::cout<<"!!!"<<tail->getdata()<<std::endl;
        tail=remove;
        tail->setNext(NULL);
        //std::cout<<"!!!"<<tail->getdata()<<std::endl;
        //std::cout<<"!!!"<<remove->getdata()<<std::endl;
        //delete remove;
    }
    else
    {
        delete tail;
        tail=NULL;
        head=NULL;
    }
}

//iterator
template <class T>
MyList<T>::iterator::iterator()
{
    current=NULL;
    last=NULL;
}
template <class T>
void MyList<T>::iterator::setcurrent(Node<T> *node)
{
    current=node;
}
template <class T>
void MyList<T>::iterator::setlast(Node<T> *node)
{
    last=node;
}
template <class T>
T MyList<T>::iterator::getcurrent()     //test
{
    return current->getdata();
}
template <class T>
T MyList<T>::iterator::getlast()         //test
{
    return last->getdata();
}
template <class T>
T MyList<T>::iterator::operator*()
{
    //std::cout<<"!!"<<std::endl;
    T temp;
    temp=current->getdata();
    return temp;
}
template <class T>
typename MyList<T>::iterator MyList<T>::iterator::operator++(int num)
{
    iterator temp;
    temp=*this;
    current=current->getNext();
    //std::cout<<"###"<<current->getdata()<<std::endl;
    return temp;         
}
template <class T>
bool MyList<T>::iterator::operator!=(iterator node)
{
    //std::cout<<"###"<<std::endl;
    bool notequal=false;
    if(current!=node.current)
    {
        notequal=true;
    }
    //std::cout<<notequal<<std::endl;
    return notequal;
}
template <class T>
typename MyList<T>::iterator MyList<T>::begin()
{
    //std::cout<<"$$"<<std::endl;
    iterator temp;
    temp.setcurrent(head);
    temp.setlast(tail);
    //std::cout<<"begin     "<<temp.getcurrent()<<" "<<temp.getlast()<<std::endl;
    return temp;
}
template <class T>
typename MyList<T>::iterator MyList<T>::end()
{
    //std::cout<<"&&"<<std::endl;
    iterator temp;
    temp.setcurrent(NULL);
    temp.setlast(tail);
    //std::cout<<"end    "<<" "<<temp.getlast()<<std::endl;
    return temp;
}
template <class T>
void MyList<T>::print(std::ostream &out)
{
    iterator it=this->begin();
    while(it!=this->end())
    {
        out<<*it<<" ";
        it++;
    }
    out<<std::endl;
}
#endif /* MyList_h */









