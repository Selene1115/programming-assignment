/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#include <iostream>
#include "MyList.h"
using namespace std;

int main()
{
    cout<<"Push integers 0, ..., 10 to the back of a linked list"<<endl;
    MyList<int> intlist;
    for(int i=0;i<10;i++)
    {
        intlist.push_back(i);
    }
    cout<<"Display integers from the front of the linked list"<<endl;
    intlist.print(cout);
    cout<<"Push characters A, ..., Z to the back of a linked list"<<endl;
    MyList<char> charlist;
    for(int i=65;i<91;i++)
    {
        charlist.push_back(i);
    }
    cout<<"Display characters from the front of the linked list"<<endl;
    charlist.print(cout);
    cout<<"Copy a linked list with characters into another linked list."<<endl;
    MyList<char> copylist(charlist);
    cout<<"Display characters from the front of the linked list"<<endl;
    copylist.print(cout);
    string week[7]={"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
    cout<<"Push strings Monday, ..., Sunday to the back of a linked list"<<endl;
    MyList<string> stringlist;
    for(int i=0;i<7;i++)
    {
        stringlist.push_back(week[i]);
    }
    cout<<"Display strings from the front of the linked list"<<endl;
    stringlist.print(cout);
    return 0;
}
