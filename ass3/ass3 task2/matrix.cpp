/*
 written by: XINWEI JIANG
 Student number: 4951207
 */

#include "matrix.h"
#include<iostream>
#include<iomanip>
#include<cmath>
using namespace std;

template <class T>
bool Matrix<T>::size=false;

template <class T>
std::istream& operator>>(std::istream& in, Matrix<T> & m)
{
    int rows=0;
    int columns=0;
    std::cout<<"input row:";
    in>>rows;
    std::cout<<"input column:";
    in>>columns;
    Matrix<T> tmp(rows,columns);
    tmp.element=new T* [tmp.row];
    for(int i=0;i<rows;i++)
    {
        tmp.element[i]=new T [tmp.column];
    }
    for(int i=0;i<rows;i++)
    {
        for(int j=0;j<columns;j++)
        {
            std::cout<<"element: ";
            in>>tmp.element[i][j];
        }
    }
    cout<<endl;
    m=tmp;
    return in;
}
template <class T>
std::ostream& operator<<(std::ostream& out, const Matrix<T> & m)
{
    if(m.size==true)
    {
        out<<m.getrow()<<"*"<<m.getcolumn()<<" Matrix"<<endl;
    }
    for(int i=0;i<m.row;i++)
    {
        out<<"  ";
        for(int j=0;j<m.column;j++)
        {
            out<<fixed<<setprecision(1)<<setw(6)<<m.element[i][j]<<" ";
        }
        out<<std::endl;
    }
    return out;
}
template <class T>
Matrix<T>::Matrix()
{
    row=0;
    column=0;
    element=NULL;
    size=false;
}
template <class T>
Matrix<T>::Matrix(int rows,int columns)
{
    row=rows;
    column=columns;
}
template <class T>
Matrix<T>::Matrix(const Matrix& m)
{
    row=m.row;
    column=m.column;
    element=new T* [row];
    for(int i=0;i<row;i++)
    {
        element[i]=new T [column];
    }
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<column;j++)
        {
            element[i][j]=m.element[i][j];
        }
    }
}
template <class T>
Matrix<T>::~Matrix()
{
    for(int i=0;i<row;i++)
    {
        delete [] element[i];
    }
    delete [] element;
    row=0;
    column=0;
}
template <class T>
int Matrix<T>::getrow() const
{
    return row;
}
template <class T>
int Matrix<T>::getcolumn() const 
{
    return column;
}
template <class T>
Matrix<T> Matrix<T>::operator=(const Matrix<T> &m)
{
    if(this==&m)
    {
        return *this;
    }
    if(element!=NULL)
    {
        for(int i=0;i<row;i++)
        {
            delete [] element[i];
        }
        delete [] element;
    }
    row=m.row;
    column=m.column;
    element=new T* [row];
    for(int i=0;i<row;i++)
    {
        element[i]=new T [column];
    }                                              //set the memory
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<column;j++)
        {
            element[i][j]=m.element[i][j];
        }
    }                                              //copy the element from m
    return *this;
}
template <class T>
Matrix<T> Matrix<T>::operator+(const Matrix& m)
{
    Matrix sum;
    if(row!=m.row||column!=m.column)
    {
        throw(string("Cannot add these two matrices"));
    }
    else
    {
        sum.row=row;
        sum.column=column;
        sum.element=new T* [sum.row];
        for(int i=0;i<sum.row;i++)
        {
            sum.element[i]=new T [sum.column];
        }
        for(int i=0;i<sum.row;i++)
        {
            for(int j=0;j<sum.column;j++)
            {
                sum.element[i][j]=element[i][j]+m.element[i][j];
            }
        }
    }
    return sum;
}
template <class T>
Matrix<T> Matrix<T>::operator-(const Matrix& m)
{
    Matrix sub;
    if(row!=m.row||column!=m.column)
    {
        throw(string("Cannot minus these two matrices"));
    }
    else
    {
        sub.row=row;
        sub.column=column;
        sub.element=new T* [sub.row];
        for(int i=0;i<sub.row;i++)
        {
            sub.element[i]=new T [sub.column];
        }
        for(int i=0;i<sub.row;i++)
        {
            for(int j=0;j<sub.column;j++)
            {
                sub.element[i][j]=element[i][j]-m.element[i][j];
            }
        }
    }
    return sub;
}
template <class T>
Matrix<T> Matrix<T>::operator*(const Matrix& m)
{
    Matrix mul;
    if(column!=m.row)
    {
        throw(string("Cannot multiple these two matrices"));
    }
    else
    {
        mul.row=row;
        mul.column=m.column;
        mul.element=new T* [mul.row];
        for(int i=0;i<mul.row;i++)
        {
            mul.element[i]=new T [mul.column];
        }
        for(int i=0;i<mul.row;i++)
        {
            for(int j=0;j<mul.column;j++)
            {
                //mul.element[i][j]=0;
                for(int k=0;k<column;k++)
                {
                    mul.element[i][j]=mul.element[i][j]+element[i][k]*m.element[k][j];
                }
            }
        }
    }
    return mul;
}


/*
 ************************************************************************
 complex number
 */

inline Complexn::Complexn()
{
    realpart=0;
    imgpart=0;
}
inline Complexn::Complexn(float real,float img)
{
    realpart=real;
    imgpart=img;
}
inline istream& operator>>(std::istream& in, Complexn &complex)
{
    float real=0;
    float img=0;
    cout<<"real: ";
    in>>real;
    cout<<"img: ";
    in>>img;
    Complexn tmp(real,img);
    complex=tmp;
    return in;
}
inline ostream& operator<<(std::ostream& out, const Complexn &complex)
{
    if(complex.printreal==true&&complex.printimg==false)
    {
        out<<complex.realpart;
    }
    if(complex.printreal==false&&complex.printimg==true)
    {
        out<<complex.imgpart;
    }
    if(complex.printimg==true&&complex.printreal==true)
    {
        out<<"("<<complex.realpart<<","<<complex.imgpart<<")";
    }
    if(complex.printmag==true)
    {
        float mag=0;
        mag=sqrt(pow(complex.realpart,2)+pow(complex.imgpart,2));
        out<<fixed<<setprecision(1)<<mag;
    }
    return out;
}
inline Complexn Complexn::operator+(const Complexn& complex)
{
    float real=0;
    float img=0;
    real=complex.realpart+realpart;
    img=complex.imgpart+imgpart;
    Complexn sum(real,img);
    return sum;
}
inline Complexn Complexn::operator-(const Complexn& complex)
{
    float real=0;
    float img=0;
    real=realpart-complex.realpart;
    img=imgpart-complex.imgpart;
    Complexn sub(real,img);
    return sub;
}
inline Complexn Complexn::operator*(const Complexn& complex)
{
    float real=0;
    float img=0;
    real=realpart*complex.realpart-imgpart*complex.imgpart;
    img=imgpart*complex.realpart+realpart*complex.imgpart;
    Complexn mul(real,img);
    return mul;
}


//************************************************************************
//manipulators
inline std::ostream& info(std::ostream& os)
{
    Matrix<float>::size=true;
    Matrix<Complexn>::size=true;
    return os;
}
inline std::ostream& noinfo(std::ostream& os)
{
    Matrix<float>::size=false;
    Matrix<Complexn>::size=false;
    return os;
}
inline std::ostream& cplx(std::ostream& os)
{
    Complexn::printreal=true;
    Complexn::printimg=true;
    Complexn::printmag=false;
    return os;
}
inline std::ostream& real(std::ostream& os)
{
    Complexn::printreal=true;
    Complexn::printimg=false;
    Complexn::printmag=false;
    return os;
}
inline std::ostream& img(std::ostream& os)
{
    Complexn::printreal=false;
    Complexn::printimg=true;
    Complexn::printmag=false;
    return os;
}
inline std::ostream& magnitude(std::ostream& os)
{
    Complexn::printreal=false;
    Complexn::printimg=false;
    Complexn::printmag=true;
    return os;
}



