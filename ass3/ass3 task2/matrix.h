/*
 written by: XINWEI JIANG
 Student number: 4951207
 */

#ifndef matrix_hpp
#define matrix_hpp

#include <iostream>

template <class T>
class Matrix;
template <class T>
std::istream& operator>>(std::istream& , Matrix<T> &);
template <class T>
std::ostream& operator<<(std::ostream& , const Matrix <T>& );

template <class T>
class Matrix
{
    friend std::istream& operator>><>(std::istream&, Matrix &);
    friend std::ostream& operator<<<>(std::ostream& , const Matrix & );
    private:
        int row;
        int column;
        T **element;
        static bool size;
    public:
        //static bool size;
        Matrix();
        Matrix(int ,int );
        Matrix(const Matrix&);
        ~Matrix();
        int getrow() const;
        int getcolumn() const;
        Matrix operator=(const Matrix&);
        Matrix operator+(const Matrix&);
        Matrix operator-(const Matrix&);
        Matrix operator*(const Matrix&);
        friend std::ostream& info(std::ostream& );
        friend std::ostream& noinfo(std::ostream& );
};

class Complexn
{
    friend std::istream& operator>>(std::istream&, Complexn &);
    friend std::ostream& operator<<(std::ostream& , const Complexn & );
    private:
        float realpart;
        float imgpart;
        static bool printreal;
        static bool printimg;
        static bool printmag;
    public:
        Complexn();
        Complexn(float ,float);
        Complexn operator+(const Complexn&);
        Complexn operator-(const Complexn&);
        Complexn operator*(const Complexn&);
    friend std::ostream& cplx(std::ostream& );
    friend std::ostream& real(std::ostream& );
    friend std::ostream& img(std::ostream& );
    friend std::ostream& magnitude(std::ostream& );
};

#endif /* matrix_hpp */
