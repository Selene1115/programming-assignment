/*
 written by: XINWEI JIANG
 Student number: 4951207
 */

#include <iostream>
#include "matrix.cpp"
#include <iomanip>
#include <fstream>
using namespace std;

bool Complexn::printreal=false;
bool Complexn::printimg=false;
bool Complexn::printmag=false;

int main()
{
    Matrix<float> m1;
    Matrix<float> m2;
//    ifstream fin;
//    fin.open("/Users/jiangxinwei/Desktop/testmatrix.txt");
//    if(fin.good()){
//        cout<<"good"<<endl;
//    }
//    else
//    {
//        cout<<"error"<<endl;
//    }
    cin>>m1;
    cout<<info<<m1<<endl;
    cin>>m2;
    cout<<info<<m2<<endl;
    Matrix<float> m3;
    Matrix<float> m4;
    Matrix<float> m5;
    try
    {
        m3=m1+m2;         //addition
        cout<<"Addition is: "<<endl;
        cout<<noinfo<<m3<<endl;
    }
    catch (string err)
    {
        cout<<"error: "<<err<<endl;
    }
    
    try
    {
        m4=m1-m2;         //subtraction
        cout<<"Subtraction is: "<<endl;
        cout<<noinfo<<m4<<endl;
    }
    catch (string err)
    {
        cout<<"error: "<<err<<endl;
    }
    
    try
    {
        m5=m1*m2;         //multiplication
        cout<<"multiplication is: "<<endl;
        cout<<info<<m5<<endl;
    }
    catch (string err)
    {
        cout<<"error: "<<err<<endl;
    }

    Matrix<Complexn> m6;             //the type of element id complex number
    Matrix<Complexn> m7;
    cin>>m6;
    cout<<info<<cplx<<m6<<endl;
    cin>>m7;
    cout<<info<<cplx<<m7<<endl;
    
    Matrix<Complexn> m8;
    Matrix<Complexn> m9;
    Matrix<Complexn> m10;
    try
    {
        m8=m6+m7;         //addition
        cout<<"Addition is: "<<endl;
        cout<<"real part: "<<endl;
        cout<<noinfo<<real<<m8<<endl;
        cout<<"imaginary part: "<<endl;
        cout<<noinfo<<img<<m8<<endl;
        cout<<"magnitude g is: "<<endl;
        cout<<noinfo<<magnitude<<m8<<endl;
    }
    catch (string err)
    {
        cout<<"error: "<<err<<endl;
    }
    
    try
    {
        m9=m6-m7;         //subtraction
        cout<<"Subtraction is: "<<endl;
        cout<<"real part: "<<endl;
        cout<<noinfo<<real<<m9<<endl;
        cout<<"imaginary part: "<<endl;
        cout<<noinfo<<img<<m9<<endl;
        cout<<"magnitude g is: "<<endl;
        cout<<noinfo<<magnitude<<m9<<endl;
    }
    catch (string err)
    {
        cout<<"error: "<<err<<endl;
    }
    
    try
    {
        m10=m6*m7;         //multiplication
        cout<<"Multiplication is: "<<endl;
        cout<<"real part: "<<endl;
        cout<<noinfo<<real<<m10<<endl;
        cout<<"imaginary part: "<<endl;
        cout<<noinfo<<img<<m10<<endl;
        cout<<"magnitude g is: "<<endl;
        cout<<noinfo<<magnitude<<m10<<endl;
    }
    catch (string err)
    {
        cout<<"error: "<<err<<endl;
    }

    return 0;
}
