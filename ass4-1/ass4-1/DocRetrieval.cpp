/*
 written by: XINWEI JIANG
 student number:4951207
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <string>
#include <cctype>
#include <cmath>
#include <iomanip>
using namespace std;

int main(int argc, const char * argv[])
{
    argc=5;
    vector<string> keywords;             //stroe the keywords
    string content;
    double dnum=0;                      //the number of words in the document
    double knum=0;                      //the number of keywords
    vector<double> relevance;
    double rel;
//    argv[1]="kyle";
//    argv[2]="radio";
//    argv[3]="2Day";
//    argv[4]="girl";
    for(int i=1;i<argc;i++)
    {
        string key=argv[i];
        for(int i=0;i<key.size();i++)
        {
            key[i]=tolower(key[i]);
        }
        keywords.push_back(key);
    }
    vector<string> filename;             //store the filename which is in the txt file
    string name;
    ifstream fin;
    fin.open("listofdocs.txt");
    if(fin.good())
    {
        while(!fin.eof())
        {
            fin>>name;
            if(fin.eof())
            {
                continue;
            }
            filename.push_back(name);
        }
        fin.close();
        for(int i=0;i<filename.size();i++)
        {
            ifstream myfile;
            myfile.open(filename[i].c_str());
            if(myfile.good())
            {
                vector<string> words;
                while(!myfile.eof())
                {
                    myfile>>content;
                    for(int k=0;k<content.size();k++)
                    {
                        content[k]=tolower(content[k]);
                        if(ispunct(content[k]))
                        {
                            content.erase(k);
                            //content[k]='\0';
                        }
                    }
                    if(myfile.eof())
                    {
                        continue;
                    }
                    words.push_back(content);
                }
                dnum=(int)words.size();
                knum=(int)keywords.size();
                double samenum=0;                  //the number of the same words
                for(int k=0;k<knum;k++)
                {
                    for(int j=0;j<dnum;j++)
                    {
                        if(keywords[k]==words[j])
                        {
                            samenum++;
                        }
                    }
                }
                rel=samenum*100/(sqrt(dnum)*sqrt(knum));
                relevance.push_back(rel);
            }
            myfile.close();
        }

       //display in descending order
        for(int i=0;i<relevance.size();i++)
        {
            int max=0;                      //find the largest relevance
            for(int j=i+1;j<relevance.size();j++)
            {
                if(relevance[i]<relevance[j])
                {
                    max=j;
                }
                swap(relevance[i],relevance[max]);
                swap(filename[i],filename[max]);
            }
        }
        for(int i=0;i<relevance.size();i++)
        {
            cout<<"("<<filename[i]<<" - "<<fixed<<setprecision(2)<<relevance[i]<<"%)  ";
            ifstream in;
            in.open(filename[i].c_str());
            if(in.good())
            {
                vector<string> words;
                while(!in.eof())
                {
                    in>>content;
                    if(in.eof())
                    {
                        continue;
                    }
                    words.push_back(content);
                }
                for(int i=0;i<9;i++)
                {
                    cout<<words[i]<<" ";
                }
                int j=int(words[9].size())-1;
                if(ispunct(words[9][j]))
                {
                    words[9].erase(j,1);
                }
                cout<<words[9]<<" ..."<<endl;
            }
            in.close();
        }
    }
    else
    {
        cout<<"the file is wrong! "<<endl;
        return -1;
    }
    return 0;
}
