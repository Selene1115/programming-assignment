/*
 written by: XINWEI JIANG
 student number: 4951207
 */

#include <iostream>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <map>
#include <iterator>
#include <string>
#include <iomanip>
using namespace std;

int main(int argc, const char * argv[])
{
    string moviename;
    float rating;
    int num=0;                  //the number of ratings in the file
    ifstream fin;
    const string delims("\r");
    float avg=0;
    
    for(int i=1;i<argc;i++)
    {
        fin.open(argv[i]);
        if(fin.good())
        {
            multimap<string, float> movie;
            pair<string, float> mypair;
            map<string, float> rresult;  //for the filename and average rating
            map<string, int> cresult;  //for the filename and the number of review
            fin>>num;
            cout<<num<<endl;
            for(int i=0;i<num;i++)
            {
                fin.ignore(5,'\n');
                getline(fin,moviename);
//                cout<<moviename.size()<<endl;                //another way to judge the delim
//                int a=int(moviename.size());
//                if(moviename[a-1]=='\r')
//                {
//                    moviename.erase(a-1);
//                }
//                cout<<"!!!!"<<moviename.size()<<endl;
                string::size_type begIdx = 0, endIdx;
                while(begIdx!=string::npos)
                {
                    endIdx=moviename.find_first_of (delims,begIdx);
                    if(endIdx==string::npos)
                    {
                        endIdx=moviename.size();
                    }
                    else
                    {
                        moviename.erase(endIdx,1);
                    }
                    begIdx = moviename.find_first_not_of (delims, endIdx);
                }
                fin>>rating;
//                mypair.first=moviename;
//                mypair.second=rating;
                movie.insert(std::pair<const std::string, float>(moviename, rating));
            }
            
            int count;
            pair<string,int> cpair;
            for(multimap<string, float>::iterator it=movie.begin();it!=movie.end();it++)
            {
                avg=0;
                count=0;
                for(multimap<string, float>::iterator ita=movie.begin();ita!=movie.end();ita++)
                {
                    if(it->first==ita->first)
                    {
                        avg=avg+ita->second;
                        count++;
                    }
                }
                avg=avg/count;
                mypair.first=it->first;
                mypair.second=avg;
                rresult.insert(mypair);
                cpair.first=it->first;
                cpair.second=count;
                cresult.insert(cpair);
            }
            
            map<string, int>::iterator ita=cresult.begin();
            for(map<string, float>::iterator it=rresult.begin();it!=rresult.end();it++)
            {
                cout<<it->first<<": "<<ita->second;
                if(ita->second>1)
                {
                    cout<<" reviews, ";
                }
                else
                {
                    cout<<" review, ";
                }
                cout<<"average of "<<setprecision(2)<<it->second<<"/5"<<endl;
                ita++;
            }
        }
        else
        {
            cout<<"wrong file"<<endl;
        }
        fin.close();
    }
    return 0;
}
