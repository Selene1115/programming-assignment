/*
 Student number:4951207
 Name: Xinwei Jiang
 */

#include "Account.h"
#include<iostream>
#include<cstring>
#include<iomanip>
using namespace std;

Account::Account()
{
    day=0;
    month=0;
    year=0;
    strcpy(BSB,"\0");
    strcpy(acc, "\0");
    strcpy(name, "\0");
    strcpy(address, "\0");
    strcpy(phone,"\0");
    strcpy(balance,"\0");
}

void Account::setBSB(char Bnum[])
{
    strcpy(BSB,Bnum);
}

void Account::setacc(char anum[])
{
    strcpy(acc,anum);
}

void Account::setname(char n[])
{
    strcpy(name,n);
}

void Account::setadd(char add[])
{
    strcpy(address,add);
}

void Account::setphone(char pnum[])
{
    strcpy(phone,pnum);
}

void Account::setdate(int d,int m,int y)
{
    day=d;
    month=m;
    year=y;
}

void Account::setbalance(char b[])
{
    strcpy(balance,b);
}

char* Account::getBSB()
{
    return BSB;
}

char* Account::getacc()
{
    return acc;
}

char* Account::getname()
{
    return name;
}

char* Account::getadd()
{
    return address;
}

char* Account::getphone()
{
    return phone;
}

char* Account::getbalance()
{
    return balance;
}

int Account::getday()
{
    return day;
}

int Account::getmonth()
{
    return month;
}

int Account::getyear()
{
    return year;
}

void Account::print(ostream &out)
{
    out<<BSB<<'\t'<<acc<<'\t'<<name<<'\t'<<address<<'\t'<<setfill('0')<<setw(2)<<day<<'/'
    <<setfill('0')<<setw(2)<<month<<'/'<<year<<'\t'<<phone<<'\t'<<balance<<endl;
}



