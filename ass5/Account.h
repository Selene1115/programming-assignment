/*
 Student number:4951207
 Name: Xinwei Jiang
 */

#ifndef Account_h
#define Account_h

#include <iostream>

class Account
{
private:
    char BSB[10];
    char acc[10];
    char name[50];
    char address[100];
    char phone[20];
    int day;
    int month;
    int year;
    char balance[10];
public:
    Account();
    void setBSB(char []);
    void setacc(char []);
    void setname(char []);
    void setadd(char []);
    void setphone(char []);
    void setdate(int, int, int);
    void setbalance(char []);
    char *getBSB();
    char *getacc();
    char *getname();
    char *getadd();
    char *getphone();
    int getday();
    int getmonth();
    int getyear();
    char *getbalance();
    void print(std::ostream &);
};

#endif /* Account_h */
