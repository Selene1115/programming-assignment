/*
 Student number:4951207
 Name: Xinwei Jiang
 */

#include "accTree.h"
#include<iostream>
#include<fstream>
#include<cstdlib>
#include<ctime>
using namespace std;

AccNode::AccNode()
{
    left=NULL;
    right=NULL;
}
void AccNode::setLeft(AccNode *L)
{
    left=L;
}
void AccNode::setRight(AccNode *R)
{
    right=R;
}
void AccNode::setData(const Account &data)
{
    object=data;
}
AccNode* AccNode::getLeft() const
{
    return left;
}
AccNode* AccNode::getRight() const
{
    return right;
}
Account AccNode::getData() const
{
    return object;
}
bool AccNode::isLeaf() const
{
    bool leaf=false;
    if(left==NULL&&right==NULL)
    {
        leaf=true;
    }
    return leaf;
}

AccTree::AccTree()
{
    root=NULL;
}
void AccTree::clear(AccNode *p)
{
    if(p!=NULL)
    {
        clear(p->getLeft());
        clear(p->getRight());
        delete p;
    }
}
AccTree::~AccTree()
{
    clear(root);
}
int AccTree::countRecords(ifstream &file)
{
    char line[256];
    int count=0;
    while(!file.eof())
    {
        file.getline(line,256);
        if (strlen(line) != 0)
        {
            count++;
        }
    }
    return count;
}
void AccTree::loadRecords(ifstream &file)
{
    file.clear();
    file.seekg(0,ios::beg);
    Account record;
    char Bnum[10];
    char anum[10];
    char n[50];
    char add[100];
    char pnum[20];
    char dd[3];
    char mm[3];
    char yyyy[5];
    int d;
    int m;
    int y;
    char b[10];
    while(!file.eof())
    {
        file.getline(Bnum,10,'\t');
        record.setBSB(Bnum);
        file.getline(anum,10,'\t');
        record.setacc(anum);
        file.getline(n,50,'\t');
        record.setname(n);
        file.getline(add,100,'\t');
        record.setadd(add);
        file>>pnum;
        record.setphone(pnum);
        file.ignore();
        file.getline(dd,3,'/');
        d=atoi(dd);
        file.getline(mm,3,'/');
        m=atoi(mm);
        file.getline(yyyy,5,'\t');
        y=atoi(yyyy);
        record.setdate(d,m,y);
        file.getline(b,10);
        record.setbalance(b);
        if(file.eof())
        {
            continue;
        }
        insert(record);
    }
}
void AccTree::insert(Account & entry)
{
    AccNode *node=new AccNode;
    node->setData(entry);
    if(root==NULL)
    {
        root = node;
    }
    else
    {
        char BST1[20];     //The BST of entry;
        char BST2[20];     //The BST of the node in the tree
        char a1[10];
        char a2[10];
        strcpy(BST1, entry.getBSB());
        strcpy(a1,entry.getacc());
        strcat(BST1,a1);
        AccNode *tmp=root;
        AccNode *parent=NULL;
        while(tmp!=NULL)
        {
            parent=tmp;
            strcpy(BST2, tmp->getData().getBSB());
            strcpy(a2,tmp->getData().getacc());
            strcat(BST2,a2);
            if(strcmp(BST2,BST1)<=0)
            {
                tmp=tmp->getRight();
            }
            else
            {
                tmp=tmp->getLeft();
            }
        }
        if(strcmp(BST2,BST1)<=0)
        {
            parent->setRight(node);
        }
        else
        {
            parent->setLeft(node);
        }
    }
}
void AccTree::printRecords(std::ostream &out)
{
    inorder(root,out);
}
void AccTree::inorder(AccNode *node,std::ostream &out)
{
    if(node!=NULL)
    {
        inorder(node->getLeft(),out);
        node->getData().print(out);
        inorder(node->getRight(),out);
    }
}
void AccTree::add()
{
    Account record;
    int count=0;
    char Bnum[10];
    char anum[10];
    char n[50];
    char add[100];
    char pnum[20];
    int d;
    int m;
    int y;
    char b[10];
    cout<<"BSB number: ";
    cin>>Bnum;
    cout<<"Account number: ";
    cin>>anum;
    char BST1[20];        //account the user wants to add
    strcpy(BST1,Bnum);
    strcat(BST1,anum);
    AccNode *tmp=root;
    char BST2[20];        //accounts in the tree
    char a2[10];
    while(tmp!=NULL)
    {
        strcpy(BST2, tmp->getData().getBSB());
        strcpy(a2,tmp->getData().getacc());
        strcat(BST2,a2);
        if(strcmp(BST2,BST1)==0)
        {
            count++;
            tmp=NULL;
        }
        if(strcmp(BST2,BST1)<0)
        {
            tmp=tmp->getRight();
        }
        if(strcmp(BST2,BST1)>0)
        {
            tmp=tmp->getLeft();
        }
    }
    if(count!=0)
    {
        cout<<"Account already exists."<<endl;
    }
    else
    {
        record.setBSB(Bnum);
        record.setacc(anum);
        cin.ignore();
        cout<<"Name: ";
        cin.getline(n,50);
        record.setname(n);
        cout<<"Address: ";
        cin.getline(add,100);
        record.setadd(add);
        cout<<"Phone: ";
        cin.getline(pnum,20);
        record.setphone(pnum);
        time_t timer=time(NULL);
        tm t=*localtime(&timer);
        d=t.tm_mday;
        m=t.tm_mon+1;
        y=t.tm_year+1900;
        record.setdate(d,m,y);
        cout<<"Balance: ";
        cin>>b;
        record.setbalance(b);
        insert(record);
        cout<<"Account has been added."<<endl;
    }
}
Account AccTree::find()
{
    Account record;
    char Bnum[10];
    char anum[10];
    cout<<"BSB number: ";
    cin>>Bnum;
    cout<<"Account number: ";
    cin>>anum;
    char BST1[20];        //account the user wants to find
    strcpy(BST1,Bnum);
    strcat(BST1,anum);
    AccNode *tmp=root;
    char BST2[20];        //accounts in the tree
    char a2[10];
    bool found=false;
    while(tmp!=NULL&&!found)
    {
        strcpy(BST2, tmp->getData().getBSB());
        strcpy(a2,tmp->getData().getacc());
        strcat(BST2,a2);
        if(strcmp(BST2,BST1)==0)
        {
            record=tmp->getData();
            found=true;
        }
        if(strcmp(BST2,BST1)<0)
        {
            tmp=tmp->getRight();
        }
        if(strcmp(BST2,BST1)>0)
        {
            tmp=tmp->getLeft();
        }
    }
    if(!found)
    {
        cout<<"Account doesn't exist."<<endl;
    }
    return record;
}
void AccTree::remove()
{
    char Bnum[10];
    char anum[10];
    cout<<"BSB number: ";
    cin>>Bnum;
    cout<<"Account number: ";
    cin>>anum;
    char BST1[20];        //account the user wants to remove
    strcpy(BST1,Bnum);
    strcat(BST1,anum);
    AccNode *tmp=root;
    AccNode *parent=root;
    char BST2[20];        //accounts in the tree
    char a2[10];
    bool found=false;
    while(tmp!=NULL&&!found)
    {
        strcpy(BST2, tmp->getData().getBSB());
        strcpy(a2,tmp->getData().getacc());
        strcat(BST2,a2);
        if(strcmp(BST2,BST1)==0)
        {
            found=true;
            tmp->getData().print(cout);
        }
        if(strcmp(BST2,BST1)<0)
        {
            parent=tmp;
            tmp=tmp->getRight();
        }
        if(strcmp(BST2,BST1)>0)
        {
            parent=tmp;
            tmp=tmp->getLeft();
        }
    }
    if(!found)
    {
        cout<<"Account doesn't exists."<<endl;
    }
    else
    {
        char choice='c';
        cout<<"Do you want to delete the account(Y/N)? ";
        cin>>choice;
        char BST3[20];     //account of parent
        char a3[10];
        strcpy(BST3,parent->getData().getBSB());
        strcpy(a3,parent->getData().getacc());
        strcat(BST3,a3);
        if(choice=='Y'||choice=='y')
        {
            if(tmp->isLeaf())
            {
                if(parent==tmp)
                {
                    root=NULL;
                }
                else
                {
                    if(strcmp(BST3,BST2)<=0)
                    {
                        parent->setRight(NULL);
                    }
                    else
                    {
                        parent->setLeft(NULL);
                    }
                }
                delete tmp;
            }
            else if(tmp->getLeft()==NULL)
            {
                if(parent==tmp)
                {
                    root=tmp->getRight();
                }
                else
                {
                    if(strcmp(BST3,BST2)<=0)
                    {
                        parent->setRight(tmp->getRight());
                    }
                    else
                    {
                        parent->setLeft(tmp->getRight());
                    }
                }
                delete tmp;
            }
            else if(tmp->getRight()==NULL)
            {
                if(parent==tmp)
                {
                    root=tmp->getLeft();
                }
                else
                {
                    if(strcmp(BST3,BST2)<=0)
                    {
                        parent->setRight(tmp->getLeft());
                    }
                    else
                    {
                        parent->setLeft(tmp->getLeft());
                    }
                }
                delete tmp;
            }
            else
            {
                AccNode *rparent = tmp;
                AccNode *rp=tmp->getRight();
                while(rp->getLeft()!=NULL)
                {
                    rparent=rp;
                    rp=rp->getLeft();
                }
                tmp->setData(rp->getData());
                char BST4[20];
                char a4[10];
                strcpy(BST4,rparent->getData().getBSB());
                strcpy(a4,rparent->getData().getacc());
                strcat(BST4,a4);
                char BST5[20];
                char a5[20];
                strcpy(BST5,rp->getData().getBSB());
                strcpy(a5,rp->getData().getacc());
                strcat(BST5,a5);
                if(strcmp(BST4,BST5)>0)
                {
                    rparent->setLeft(rp->getRight());
                }
                else
                {
                    rparent->setRight(rp->getRight());
                }
                delete rp;
            }
            cout<<"Account has been deleted."<<endl;
        }
    }
}
void AccTree::save()
{
    ofstream fout;
    char file[100];
    cout<<"File name : ";
    cin>>file;
    fout.open(file);
    saveRecords(fout);
    fout.close();
    int count=0;
    ifstream fin;
    fin.open(file);
    count=countRecords(fin);
    fin.close();
    cout<<count<<" accounts have been saved."<<endl;
}
void AccTree::saveRecords(std::ostream & out)
{
    inorder(root,out);
}











