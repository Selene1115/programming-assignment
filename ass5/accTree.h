/*
 Student number:4951207
 Name: Xinwei Jiang
 */

#ifndef accTree_hpp
#define accTree_hpp

#include <iostream>
#include "Account.h"

class AccNode
{
private:
    Account object;
    AccNode *left;
    AccNode *right;
public:
    AccNode();
    void setLeft(AccNode *);
    void setRight(AccNode *);
    void setData(const Account &);
    AccNode *getLeft() const;
    AccNode *getRight() const;
    Account getData() const;
    bool isLeaf() const;
};

class AccTree
{
private:
    AccNode *root;
public:
    AccTree();
    void clear(AccNode *);
    ~AccTree();
    int countRecords(std::ifstream &);
    void loadRecords(std::ifstream &);
    void insert(Account &);
    void printRecords(std::ostream &);
    void inorder(AccNode *,std::ostream &);
    void add();
    Account find();
    void remove();
    void save();
    void saveRecords(std::ostream &);
};
#endif /* accTree_hpp */
