/*
 Student number:4951207
 Name: Xinwei Jiang
 */

#include "accTree.h"
#include<iostream>
#include<fstream>
using namespace std;

int main(int argc, char* argv[])
{
    if (argc != 2)         //argc should be 2 for correct execution
    {
        cerr<<"Something wrong"<<endl;
        return 1;
    }
    ifstream fin;
    fin.open(argv[1]);
    
    if(!fin.good())
    {
        cout << "Wrong file" << endl;
    }
    AccTree tree;
    Account record;
    int count=0;
    count=tree.countRecords(fin);
    cout<<count<<" accounts have been inserted into a BST."<<endl;
    tree.loadRecords(fin);
    fin.close();
    char choice='c';
    while(choice!='Q'&&choice!='q')
    {
        cout<<endl;
        cout<<"A. Add an account."<<endl;
        cout<<"F. Find an account."<<endl;
        cout<<"R. Remove an account."<<endl;
        cout<<"D. Display all accounts."<<endl;
        cout<<"S. Save all accounts."<<endl;
        cout<<"Q. Quit."<<endl;
        cout<<"Your choice:";
        cin>>choice;
        cin.ignore();
        
        switch(choice)
        {
            case 'A':
            case 'a':tree.add();
                break;
            case 'F':
            case 'f':record=tree.find();
                if(record.getday()!=0)
                {
                    record.print(cout);
                }
                break;
            case 'R':
            case 'r':tree.remove();
                break;
            case 'D':
            case 'd':tree.printRecords(cout);
                break;
            case 'S':
            case 's':tree.save();
                break;
            case 'Q':
            case 'q':cout<<"Bye."<<endl;
                break;
            default: cout<<"Invalid choice"<<endl;
        }
    }
}
