/*
 written by: Xinwei Jiang
 student number:4951207
 */

#include "stockHeap.h"
#include<iostream>
#include<fstream>
using namespace std;

int main(int argc, char* argv[])
{
    StockHeap recs;
    if (argc != 2)         //argc should be 2 for correct execution
    {
        cerr<<"Something wrong"<<endl;
        return 1;
    }
    ifstream file;
    file.open(argv[1]);
    if(file.good())
    {
        recs.countRecords(file);
        recs.allocateMemory();
        recs.loadRecords(file);
        cout<<"Before heap sort, the stock heap contains:"<<endl;
        recs.printRecords(cout);
        cout<<endl;
        cout<<"After heap sort, the stock heap contains:"<<endl;
        recs.sortRecords();
        recs.printRecords(cout);
    }
    else
    {
        cout<<"wrong file"<<endl;
    }
    return 0;
}