/*
 *prepared by: Xinwei Jiang  4951207
 *lab:Wednesday 13:30-15:30
 *modification data:19 August 2015
 */
#include<iostream>
#include<fstream>
#include<cstring>
#include<cstdlib>
using namespace std;

struct assMarks
{
    int stuNum;
    double marks[6];
};

void save(assMarks records[],int size,char *argv[]);
void readRecords(assMarks records[],int size, char *argv[]);
void diaplay(assMarks records[],int size);

int main(int argc, char *argv[])
{
    assMarks records[100];
    int count=0;
    char tittle[256],ass1[100][8],ass2[100][8],ass3[100][8],ass4[100][8],lab[100][8],final[100][8];
    if(argc!=3)
    {
        cerr<<"Something wrong"<<endl;
        return 1;
    }
    ifstream fin;
    fin.open(argv[1]);
    if(fin.good())
    {
        fin.getline(tittle,256);
        for(int i=0;i<100&&!fin.eof();i++)
        {
            fin>>records[i].stuNum;
            fin.ignore(1);
            fin.getline(ass1[i],8,'\t');
            if(strcmp(ass1[i]," ")==0)
            {
                records[i].marks[0]=0;
            }
            else
            {
                records[i].marks[0]=atof(ass1[i]);
            }
            fin.getline(ass2[i],8,'\t');
            if(strcmp(ass2[i]," ")==0)
            {
                records[i].marks[1]=0;
            }
            else
            {
                records[i].marks[1]=atof(ass2[i]);
            }
            fin.getline(ass3[i],8,'\t');
            if(strcmp(ass3[i]," ")==0)
            {
                records[i].marks[2]=0;
            }
            else
            {
                records[i].marks[2]=atof(ass3[i]);
            }
            fin.getline(ass4[i],8,'\t');
            if(strcmp(ass4[i]," ")==0)
            {
                records[i].marks[3]=0;
            }
            else
            {
                records[i].marks[3]=atof(ass4[i]);
            }
            fin.getline(lab[i],8,'\t');
            if(strcmp(lab[i]," ")==0)
            {
                records[i].marks[4]=0;
            }
            else
      	    {
                records[i].marks[4]=atof(lab[i]);
            }
            fin.getline(final[i],8);
            if(strcmp(final[i]," ")==0)
            {
                records[i].marks[5]=0;
            }
            else
            {
                records[i].marks[5]=atof(final[i]);
            }
            count=i;
        }
    }
    else
    {
        cout<<"wrong file"<<endl;
    }
    fin.close();
    save(records,count,argv);
    readRecords(records,count,argv);
    cout<<tittle<<"\t"<<"Total mark"<<endl;
    diaplay(records,count);
}

void save(assMarks records[],int size, char *argv[])
{
    ofstream fou;
    fou.open(argv[2],ios::out|ios::binary);
    if(fou.good())
    {
       fou.write(reinterpret_cast<char *>(records),100*sizeof(assMarks));
    }
    else
    {
	cout<<"wrong file"<<endl;
    }
    fou.close();
}

void readRecords(assMarks records[],int size, char *argv[])
{
    ifstream fin;
    fin.open(argv[2],ios::in|ios::binary);
    if(fin.good())
    {
        fin.read(reinterpret_cast<char *>(records),100*sizeof(assMarks));
    }
    else
    {
        cout<<"wrong file"<<endl;
    }
    fin.close();
}

void diaplay(assMarks records[],int size)
{
    double sum[100];
    for(int i=0;i<size;i++)
    {
        sum[i]=records[i].marks[0]+records[i].marks[1]+records[i].marks[2]+records[i].marks[3]+records[i].marks[4]+records[i].marks[5];
        cout<<records[i].stuNum<<"\t"<<records[i].marks[0]<<"\t"<<records[i].marks[1]<<"\t"<<records[i].marks[2]<<"\t"<<records[i].marks[3]<<"\t"<<records[i].marks[4]<<"\t"<<records[i].marks[5]<<"\t\t"<<sum[i]<<endl;
    }
}
