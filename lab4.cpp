/*
    written by: Xinwei Jiang
    student number: 4951207
*/

#include<iostream>
#include<cstring>
#include<fstream>
#include<cstdlib>
using namespace std;

struct stock
{
    char tag[5];
    int cost;
    long int volume;
};

void insertion(stock records[],int size);
void bubble(stock records[], int size);
void selection(stock records[], int size);

int main()
{
    stock records[100];
    int count=0;
    char choice;
    ifstream fin;
    fin.open("stockdata.txt");
    if(fin.good())
    {
        for(int i=0;i<100&&!fin.eof();i++)
        {
            fin >> records[i].tag;
            fin >> records[i].cost;
            fin >> records[i].volume;
            count=i;
        }
    }
    else
    {
        cout<<"wrong file"<<endl;
        return -1;
    }
    fin.close();
    while(choice!='t'&&choice!='T'&&choice!='q'&&choice!='Q'&&choice!='V'&&choice!='v'&&choice!='C'&&choice!='c')
    {
        cout<<"Main Menu:"<<endl;
        cout<<"t:sort data by tag."<<endl;
        cout<<"c:sort data by Cost."<<endl;
        cout<<"v:sort data by trade Volume."<<endl;
        cout<<"q:Quit."<<endl;
        cout<<"Enter Choice: ";
        cin>>choice;
    }
    switch(choice)
    {
        case 'q':
        case 'Q':return 1;
            break;
        case 't':
        case 'T': insertion(records,count);
            break;
        case 'c':
        case 'C': bubble(records,count);
            break;
        case 'v':
        case 'V':selection(records,count);
            break;
    }
    return 0;
}

void insertion(stock records[],int size)
{
    int wallIndex=1,i;
    char temp[5];
    int total;
    stock red;
    while(wallIndex<=size-1)
    {
        strcpy(temp,records[wallIndex].tag);
        i=wallIndex;
        while(i>0 && strcmp(records[i-1].tag,temp)>0)
        {
            red=records[i];
            records[i]=records[i-1];
            records[i-1]=red;
            i--;
        }
        strcpy(temp,records[i].tag);
        wallIndex++;
    }
    ofstream fou;
    fou.open("data.txt");
    if(fou.good())
    {
        fou<<"Corporate Stock Cost - NASDAQ"<<endl;
        fou<<"Stock Tag"<<endl;
        fou<<"Total cost"<<endl;
        for(int t=0;t<size;t++)
        {
            total=records[t].volume*records[t].cost;
            fou<<records[t].tag<<" "<<total<<endl;
        }
    }
}

void bubble(stock records[], int size)
{
    int wallIndex=0,i;
    stock temp;
    while(wallIndex<size-1)
    {
        i=size-1;
        while(i>wallIndex)
        {
            if(records[i].cost>records[i-1].cost)
            {
                temp=records[i];
                records[i]=records[i-1];
                records[i-1]=temp;
            }
            i--;
        }
        wallIndex=wallIndex+1;
    }
    ofstream fou;
    fou.open("data.txt");
    if(fou.good())
    {
        fou<<"Corporate Stock Cost - NASDAQ"<<endl;
        fou<<"Stock Tag"<<endl;
        fou<<"Cost"<<endl;
        for(int t=0;t<size;t++)
        {
            fou<<records[t].tag<<" "<<records[t].cost<<endl;
        }
    }
}

void selection(stock records[], int size)
{
    int wallIndex=0,minIndex,i;
    stock temp;
    while(wallIndex<size-1)
    {
        minIndex=wallIndex;
        i=wallIndex+1;
        while(i<size)
        {
            if(records[i].volume>records[minIndex].volume)
            {
                minIndex=i;
            }
            i++;
        }
        temp=records[minIndex];
        records[minIndex]=records[wallIndex];
        records[wallIndex]=temp;
        wallIndex++;
    }
    ofstream fou;
    fou.open("data.txt");
    if(fou.good())
    {
        fou<<"Corporate Stock Cost - NASDAQ"<<endl;
        fou<<"Stock Tag"<<endl;
        fou<<"Volume"<<endl;
        for(int t=0;t<size;t++)
        {
            fou<<records[t].tag<<" "<<records[t].volume<<endl;
        }
    }
}

