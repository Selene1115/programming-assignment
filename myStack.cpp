/*
 Written by: Xinwei Jiang
 Student number:4951207
 */

#include "myStack.h"

void MyStack::push(const IntPair &entry)
{
    list.push_front(entry);
}
IntPair MyStack::pop()
{
    IntPair p;
    p=list.pop_front();
    return p;
}
bool MyStack::isEmpty()
{
    bool empty;
    empty=list.isEmpty();
    return empty;
}