/*
  Written by:Xinwei jiang
  Student number:4951207
 */

#include "phonebook.h"
#include<cstring>
using namespace std;

void Contact::setName(char n[50])
{
    strcpy(name,n);
}

void Contact::setNum(char num[15])
{
    strcpy(number,num);
}

void Contact::setStr(char str[100])
{
    strcpy(street, str);
}

void Contact::setCity(char c[50])
{
    strcpy(city,c);
}

char* Contact::getName()
{
    return name;
}

char* Contact::getNum()
{
    return number;
}

char* Contact::getStr()
{
    return street;
}

char* Contact::getCity()
{
    return city;
}

Phonebook::Phonebook()
{
    size=0;
    capacity=25;
    record=new Contact[capacity];
}

Phonebook::~Phonebook()
{
    if(record!=NULL)
    {
        delete [] record;
    }
    record = NULL;
}

int Phonebook::count(std::fstream &file)
{
    char line[256];
    int count=0;
    while(!file.eof())
    {
        file.getline(line,256,'\n');
        if(strlen(line)!=0)
        {
            size++;
        }
    }
    count=size;
    cout<<size<<" contact records have been loaded."<<endl;
    return count;
}

void Phonebook::load(std::fstream &file)
{
    file.clear();
    file.seekg(0, ios::beg);
    char n[50],num[15],str[100],c[50];
    for(int i=0;i<size;i++)
    {
        file.getline(n,50,',');
        record[i].setName(n);
        file.getline(num,15,',');
        record[i].setNum(num);
        file.getline(str,100,',');
        record[i].setStr(str);
        file.getline(c,50,'\n');
        record[i].setCity(c);
    }
}

void Phonebook::add()
{
    char n[50],num[15],str[100],c[50];
    int count=0; //be used to know the record exists or not.
    //cin.ignore();
    cout<<"Company name: ";
    cin.getline(n,50,'\n');
    for(int i=0;i<size;i++)
    {
        if(strcmp(n,record[i].getName())==0)
        {
            count++;
        }
    }
    if(count!=0)
    {
        cout<<"The company already exists."<<endl;
    }
    else
    {
        cout<<"Phone: ";
        cin.getline(num,15,'\n');
        cout<<"Street: ";
        cin.getline(str,100,'\n');
        cout<<"City: ";
        cin.getline(c,50,'\n');
    }
    if(size>=capacity)
    {
                
        capacity=capacity+10;
        Contact *tmp=new Contact[capacity];
        for(int i=0;i<size;i++)
        {
            tmp[i]=record[i];
        }
        tmp[size].setName(n);
        tmp[size].setNum(num);
        tmp[size].setStr(str);
        tmp[size].setCity(c);
        delete [] tmp;
        record = tmp;
        size++;
    }
    else
    {
        record[size].setName(n);
        record[size].setNum(num);
        record[size].setStr(str);
        record[size].setCity(c);
        size++;
    }
    int wallIndex=0,minIndex,i;
    Contact temp;
    while(wallIndex<size-1)
    {
        minIndex=wallIndex;
        i=wallIndex+1;
        while(i<size)
        {
            if(strcmp(record[i].getName(),record[minIndex].getName())<0)
            {
                minIndex=i;
            }
            i++;
        }
        temp=record[minIndex];
        record[minIndex]=record[wallIndex];
        record[wallIndex]=temp;
        wallIndex++;
    }
    cout<<"Contact has been added into the phone book."<<endl;
}

void Phonebook::find(char n[50], int lower, int upper)
{
    int middle=0;
    int count=0; //be used to know the record find or not
    while(lower<=upper&&middle!=upper)//when lower=upper=middle, middle!=upper will be used to quit the while loop.
    {
        middle=(lower+upper)/2;
        if (strcmp(n,record[middle].getName())==0)
        {
            count++;
        }
        if (strcmp(n,record[middle].getName())<0)
        {
            upper=middle-1;
        }
        if(strcmp(n,record[middle].getName())>0)
        {
            lower=middle+1;
        }
    }
    if(count==0)
    {
        cout<<"The contact cannot be found"<<endl;
    }
    else
    {
        cout<<record[middle].getName()<<", "<<record[middle].getNum()<<", "<<record[middle].getStr()<<", "<<
        record[middle].getCity()<<endl;
    }
}

void Phonebook::update()
{
    char n[50],num[15],str[100],c[50];
    int count=0,find=0;
    cout<<"Company name:";
    cin.getline(n,50,'\n');
    for(int i=0;i<size;i++)
    {
        if(strcmp(n,record[i].getName())==0)
        {
            find=i;
            count++;
        }
    }
    if(count!=0)
    {
        cout<<record[find].getName()<<", "<<record[find].getNum()<<", "<<record[find].getStr()<<", "<<record[find].getCity()<<endl;
        cout<<"Phone: ";
        cin.getline(num,15,'\n');
        cout<<"Street: ";
        cin.getline(str,100,'\n');
        cout<<"City: ";
        cin.getline(c,50,'\n');
        record[find].setNum(num);
        record[find].setStr(str);
        record[find].setCity(c);
        cout<<"Contact has been updated."<<endl;
    }
    else
    {
        cout<<"Contact cannot be found."<<endl;
    }
}

void Phonebook::remove()
{
    char n[50],choice;
    int count=0,find=0,line=size;
    cout<<"Company name:";
    cin.getline(n,50,'\n');
    for(int i=0;i<size;i++)
    {
        if(strcmp(n,record[i].getName())==0)
        {
            find=i;
            count++;
        }
    }
    if(count!=0)
    {
        cout<<record[find].getName()<<", "<<record[find].getNum()<<", "<<record[find].getStr()<<", "<<record[find].getCity()<<endl;
        cout<<"Do you want to remove it? (Y/N)";
        cin>>choice;
        if(choice=='y'||choice=='Y')
        {
            for(int i=find;i<line-1;i++)
            {
                record[i]=record[i+1];
                line--;                      //line is uesd to make the size cannot be changed.
            }
            size=size-1;
            cout<<"The contact has been removed."<<endl;
        }
    }
    else
    {
        cout<<"Contact cannot be found."<<endl;
    }
}

void Phonebook::save(std::ofstream &file)
{
    for(int i=0;i<size;i++)
    {
        file<<record[i].getName()<<",";
        file<<record[i].getNum()<<",";
        file<<record[i].getStr()<<",";
        file<<record[i].getCity()<<endl;
    }
    cout<<size<<" contact records have been loaded."<<endl;
}

void Phonebook::print(std::ostream &out) const
{
    for(int i=0;i<size;i++)
    {
        out<<record[i].getName()<<","<<record[i].getNum()<<","<<record[i].getStr()<<","<<record[i].getCity()<<endl;
    }
}



































