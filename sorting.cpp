/*
 written by: Xinwei Jiang
 student number:4951207
 */

#include "sorting.h"
#include<iostream>
using namespace std;

void quickSort(data record[],int size)
{
    int pivotIndex=0,l=0,h=0;
    if(size>1)
    {
        char pivotName[30];
        int low=1, high=size-1;
        data pivot=record[0],temp;
        strcpy(pivotName,record[0].name);
        while(low<=high)
        {
            while(strcmp(pivotName,record[low].name)>=0 && low<size)
            {
                low++;
            }
            while(strcmp(pivotName, record[high].name)<0)
            {
                high--;
            }
            if(low<high)
            {
                temp=record[low];
                record[low]=record[high];
                record[high]=temp;
            }
        }
        pivotIndex=high;
        record[0]=record[pivotIndex];
        record[pivotIndex]=pivot;
        l=pivotIndex;
        h=size-l-1;
        quickSort(record,l);
        quickSort((record+pivotIndex+1),h);
    }
}

void insertion(data record[],int size)
{
    int wallIndex=1,i;
    int temp;
    data red;
    while(wallIndex<size)
    {
        temp=record[wallIndex].age;
        i=wallIndex;
        while(i>0 && record[i-1].age<temp)
        {
            red=record[i];
            record[i]=record[i-1];
            record[i-1]=red;
            i--;
        }
        temp=record[i].age;
        wallIndex++;
    }
}

void selection(data record[],int size)
{
    int wallIndex=0,maxIndex,i;
    data temp;
    while(wallIndex<size-1)
    {
        maxIndex=wallIndex;
        i=wallIndex+1;
        while(i<size)
        {
            if(record[i].balance>record[maxIndex].balance)
            {
                maxIndex=i;
            }
            i++;
        }
        temp=record[maxIndex];
        record[maxIndex]=record[wallIndex];
        record[wallIndex]=temp;
        wallIndex++;
    }
}
