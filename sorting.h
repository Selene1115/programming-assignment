/*
 written by: Xinwei Jiang
 student number:4951207
 */

#include "text2bin.h"
#ifndef sorting_H_
#define sorting_H_

void quickSort(data record[],int size);
void insertion(data record[],int size);
void selection(data record[],int size);
#endif
