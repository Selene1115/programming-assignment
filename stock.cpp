//
//  stock.cpp
//  lab8
//
//  Created by jiangxinwei on 23/09/2015.
//  Copyright © 2015 jiangxinwei. All rights reserved.
//

#include "stock.h"
#include<iostream>

void Stock::setTag(char Tag[])
{
    strcpy(tag,Tag);
}

void Stock::setCost(int Cost)
{
    cost=Cost;
}

void Stock::setVolume(long Volume)
{
    volume=Volume;
}

char* Stock::getTag()
{
    return tag;
}

int Stock::getCost()
{
    return cost;
}

long int Stock::getVolume()
{
    return volume;
}