//
//  stock.h
//  lab8
//
//  Created by jiangxinwei on 23/09/2015.
//  Copyright © 2015 jiangxinwei. All rights reserved.
//

#ifndef stock_hpp
#define stock_hpp

class Stock
{
private:
    char tag[5];
    int cost;
    long int volume;
public:
    void setTag(char []);
    void setCost(int);
    void setVolume(long int);
    char* getTag();
    int getCost();
    long int getVolume();
};

#endif /* stock_hpp */