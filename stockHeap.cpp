//
//  stockHeap.cpp
//  lab10
//
//  Created by jiangxinwei on 21/10/2015.
//  Copyright © 2015 jiangxinwei. All rights reserved.
//

#include "stockHeap.h"
#include<cstring>
#include<iostream>
using namespace std;

StockHeap::StockHeap()
{
    stocks=NULL;
    size=0;
}

StockHeap::~StockHeap()
{
    if(stocks!=NULL)
        delete [] stocks;
}

int StockHeap::countRecords(std::ifstream &file)
{
    char line[256];
    int num=0;
    while(!file.eof())
    {
        file.getline(line,256);
        if(file.eof())
        {
            continue;
        }
        if(strcmp(line," ")!=0)
        {
            num++;
        }
    }
    size=num;
    return num;
}

void StockHeap::allocateMemory()
{
    stocks = new Stock[size];
}

void StockHeap::loadRecords(std::ifstream &file)
{
    file.clear();
    file.seekg(0, ios::beg);
    char Tag[5];
    int Cost;
    long int Volume;
    for(int i=0;i<size;i++)
    {
        file>>Tag;
        stocks[i].setTag(Tag);
        file>>Cost;
        stocks[i].setCost(Cost);
        file>>Volume;
        stocks[i].setVolume(Volume);
    }
}

void StockHeap::makeheap(int heapsize)
{
    for(int i=(heapsize-2)/2;i>=0;i--)      //start from the last parent
    {
        max_heapify(i,heapsize);
    }
}

void StockHeap::max_heapify(int i, int heapsize)    //找出最大的数
{
    int left=2*i+1;             //left child
    int right=2*i+2;            //right child
    int largest = i;            //parent,在这个subtree中，parent是最大的
    if(left<heapsize && strcmp(stocks[left].getTag(),stocks[i].getTag())>0)
    {
        largest=left;
    }
    if(right<heapsize && strcmp(stocks[right].getTag(),stocks[largest].getTag())>0)
    {
        largest = right;
    }
    if(largest!=i)            //说明最大的不是parent
    {
        swap(stocks[i],stocks[largest]);
        max_heapify(largest, heapsize);
    }
}

void StockHeap::sortRecords()
{
    int unsorted;
    makeheap(size);         //把最大的放到了最前面
    unsorted=size;
    while(unsorted>1)
    {
        unsorted--;
        swap(stocks[0],stocks[unsorted]);       //交换第一个和最后一个
        makeheap(unsorted);                     //在未排序的数列里排序，找出最大的数
    }
}

void StockHeap::swap(Stock &big, Stock &small)
{
    Stock temp;
    temp=big;
    big=small;
    small=temp;
}

void StockHeap::printRecords(std::ostream &out)
{
    for(int i=0;i<size;i++)
    {
        out<<stocks[i].getTag()<<"\t"<<stocks[i].getCost()<<"\t"<<stocks[i].getVolume()<<endl;
    }
}







