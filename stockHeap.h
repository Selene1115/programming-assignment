//
//  stockHeap.h
//  lab10
//
//  Created by jiangxinwei on 21/10/2015.
//  Copyright © 2015 jiangxinwei. All rights reserved.
//

#ifndef stockHeap_h
#define stockHeap_h

#include "stock.h"
#include<fstream>

class StockHeap
{
private:
    Stock *stocks;
    int size;
public:
    StockHeap();
    int countRecords(std::ifstream &file);
    void allocateMemory();
    void loadRecords(std::ifstream &file);
    void makeheap(int size);
    void max_heapify(int, int);
    void sortRecords();
    void swap(Stock &, Stock &);
    void printRecords(std::ostream &out);
    ~StockHeap();
};

#endif /* stockHeap_h */
