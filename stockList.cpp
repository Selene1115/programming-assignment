/*
 written by: Xinwei Jiang
 student number: 4951207
 */


#include "stockList.h"
#include<fstream>
#include<iostream>

StockNode::StockNode()
{
    next = NULL;
}

void StockNode::setValue(const Stock &entry)
{
    value=entry;
}

Stock StockNode::getValue()
{
    return value;
}

void StockNode::setNext(StockNode *ptr)
{
    next=ptr;
}

StockNode *StockNode::getNext()
{
    return next;
}

StockList::StockList()
{
    head = NULL;
}

StockList::~StockList()
{
    StockNode *nodePtr;
    StockNode *nextNode;
    
    nodePtr=head;
    while ((nodePtr!=NULL))
    {
        nextNode=nodePtr->getNext();
        delete nodePtr;
        nodePtr=nextNode;
    }
}

void StockList::addRecords(Stock &record)
{
    bool empty=isEmpty();
    StockNode *addPtr;
    StockNode *node;
    addPtr=new StockNode;
    addPtr->setValue(record);
    addPtr->setNext(NULL);
    if(empty)
    {
        head=addPtr;
    }
    else
    {
        node=head;
        while(node->getNext()!=NULL)
        {
            node=node->getNext();
        }
        node->setNext(addPtr);
    }
}

StockList::StockList(const StockList &obj)
{
    StockNode *node;
    node=obj.head;
    head=NULL;
    
    while(node)
    {
        Stock st;
        st=node->getValue();
        addRecords(st);
        node=node->getNext();
    }
}

void StockList::loadRecords(std::ifstream &file)
{
    Stock record;
    char Tag[5];
    int Cost;
    long int Volume;
    while(!file.eof())
    {
        file>>Tag;
        record.setTag(Tag);
        file>>Cost;
        record.setCost(Cost);
        file>>Volume;
        record.setVolume(Volume);
        if(file.eof())
        {
            continue;
        }
        addRecords(record);
    }
}

bool StockList::isEmpty()
{
    bool empty=false;
    if(head==NULL)
    {
        empty=true;
    }
    return empty;
}

void StockList::printRecords(std::ostream &out)
{
    StockNode *node;
    node=head;
    while(node)
    {
        out<<node->getValue().getTag()<<"\t";
        out<<node->getValue().getCost()<<"\t";
        out<<node->getValue().getVolume()<<std::endl;
        node=node->getNext();
    }
}















