/*
 written by: Xinwei Jiang
 student number:4951207
 */

#include "stockTree.h"
#include<iostream>

StockNode::StockNode()
{
    left=NULL;
    right=NULL;
}
void StockNode::setLeft(StockNode *L)
{
    left=L;
}
void StockNode::setRight(StockNode *R)
{
    right=R;
}
void StockNode::setData(Stock &data)
{
    object=data;
}
StockNode* StockNode::getLeft() const
{
    return left;
}
StockNode* StockNode::getRight() const
{
    return right;
}
Stock StockNode::getData() const
{
    return object;
}
bool StockNode::isLeaf() const
{
    bool leaf=false;
    if(left==NULL&&right==NULL)
    {
        leaf=true;
    }
    return leaf;
}

StockTree::StockTree()
{
    root=NULL;
}
StockTree::~StockTree()
{
    deleteRecords(root);
}
StockTree::StockTree(const StockTree &obj)
{
    StockNode *node;
    node=obj.root;
    root=NULL;

    copy(node);
}
void StockTree::copy(StockNode *node)
{
    if(node!=NULL)
    {
        Stock st;
        st=node->getData();
        insertRecords(st);
        copy(node->getLeft());
        copy(node->getRight());
    }
}
void StockTree::loadRecords(std::ifstream &file)
{
    Stock record;
    char Tag[5];
    int Cost;
    long int Volume;
    while(!file.eof())
    {
        file>>Tag;
        record.setTag(Tag);
        file>>Cost;
        record.setCost(Cost);
        file>>Volume;
        record.setVolume(Volume);
        if(file.eof())
        {
            continue;
        }
        insertRecords(record);
    }
}
void StockTree::deleteRecords(StockNode *node)
{
    if(node!=NULL)
    {
        deleteRecords(node->getLeft());
        deleteRecords(node->getRight());
        delete node;
    }
}
void StockTree::insertRecords(Stock &entry)
{
    StockNode *node=new StockNode;
    node->setData(entry);
    if(root==NULL)
    {
        root = node;
    }
    else
    {
        StockNode *tmp = root;
        StockNode *parent = NULL;
        
        while(tmp!=NULL)
        {
            parent = tmp;
            if(strcmp(tmp->getData().getTag(),entry.getTag())<=0)
            {
                tmp=tmp->getRight();
            }
            else
            {
                tmp=tmp->getLeft();
            }
        }
        if(strcmp(parent->getData().getTag(),entry.getTag())<=0)
        {
            parent->setRight(node);
        }
        else
        {
            parent->setLeft(node);
        }
    }
}
bool StockTree::isEmpty()
{
    bool empty=false;
    if(root==NULL)
    {
        empty=true;
    }
    return empty;
}
void StockTree::printRecords(std::ostream &out)
{
    inorder(root,out);
}
void StockTree::inorder(StockNode *node,std::ostream &out)
{
    if(node!=NULL)
    {
        inorder(node->getLeft(),out);
        out<<node->getData().getTag()<<"\t"<<node->getData().getCost()<<"\t"<<node->getData().getVolume()<<endl;;
        inorder(node->getRight(),out);
    }
}










