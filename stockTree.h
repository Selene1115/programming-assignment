/*
 written by: Xinwei Jiang
 student number:4951207
 */

#ifndef stockTree_hpp
#define stockTree_hpp

#include "stock.h"

class StockNode
{
private:
    Stock object;
    StockNode *left;
    StockNode *right;
public:
    StockNode();
    void setLeft(StockNode *);
    void setRight(StockNode *);
    void setData(Stock &);
    StockNode *getLeft() const;
    StockNode *getRight() const;
    Stock getData() const;
    bool isLeaf() const;
};

class StockTree
{
private:
    StockNode *root;
public:
    StockTree();
    ~StockTree();
    StockTree(const StockTree &);
    void loadRecords(std::ifstream &);
    void printRecords(std::ostream &);
    void deleteRecords(StockNode *);
    void insertRecords(Stock &);
    void copy(StockNode *);
    void inorder(StockNode *,std::ostream &);
    bool isEmpty();
};

#endif /* stockTree_hpp */
