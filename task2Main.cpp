/*
 Written by: Xinwei Jiang
 Student number:4951207
 */

#include "myStack.h"
#include<iostream>
#include<ctime>
#include<cstdlib>
using namespace std;

int main()
{
    MyStack stack;
    IntPair p,front;
    int l,r,fl,fr;
     
    srand (unsigned(time(NULL)));
    for(int i=0;i<10;i++)
    {
        l=rand()%100;
        r=rand()%100;
        p.setLeft(l);
        p.setRight(r);
        cout<<"Push ("<<l<<","<<r<<")"<<endl;
        stack.push(p);
    }

    cout<<endl;
     
    for(int i=0;i<10;i++)
    {
        front=stack.pop();
        fl=front.getLeft();
        fr=front.getRight();
        cout<<"Pop ("<<fl<<","<<fr<<")"<<endl;
    }
    
    if(!stack.isEmpty())
    {
        cout<<"The stack is not empty."<<endl;
    }
    return 0;
}