/*
 Written by:Xinwei jiang
 Student number:4951207
 */

#include<iostream>
#include<fstream>
#include<cstring>
#include "phonebook.h"
using namespace std;

int main()
{
    char txt[50],choice='c';
    Phonebook recs;
    int count=0;
    cout<<"Input a phonebook file: ";
    cin>>txt;
    fstream file;
    ofstream fout;
    file.open(txt,ios::in|ios::out);
    if(file.good())
    {
        count=recs.count(file);
        //cout<<count<<endl;
        recs.load(file);
        int lower=0,upper=count-1;
        while(choice!='Q'&&choice!='q')
        {
            cout<<endl;
            cout<<"A. Add a new contact."<<endl;
            cout<<"F. Find a contact."<<endl;
            cout<<"U. Update a contact."<<endl;
            cout<<"D. Display all contacts."<<endl;
            cout<<"R. Remove a contact."<<endl;
            cout<<"Q. Quit."<<endl;
            cout<<"Please choose:";
            cin>>choice;
            cin.ignore();
            
            switch(choice)
            {
                case 'A':
                case 'a':recs.add();
                    break;
                case 'F':
                case 'f':char n[50];
                    cout<<"Company name: ";
                    cin.getline(n,50,'\n');
                    recs.find(n,lower,upper);
                    break;
                case 'U':
                case 'u':recs.update();
                    break;
                case 'D':
                case 'd':recs.print(cout);
                    break;
                case 'R':
                case 'r':recs.remove();
                    break;
                case 'Q':
                case 'q':
                    fout.open(txt);
                    recs.save(fout);
                    cout<<"Bye."<<endl;
                    break;
                default: cout<<"Invalid choice"<<endl;
            }
        }
    }
    else
    {
        cout<<"Wrong file."<<endl;
        return -1;
    }
    file.close();
    return 0;
}